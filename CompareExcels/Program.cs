﻿using Aspose.Cells;
using DiffMatchPatch;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace CompareExcels
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            while (true)
            {



                Console.WriteLine("Copy and paste location of the Original (Before) file");
                string @before = Console.ReadLine();
                before = before.Replace("\"", "");


                Console.WriteLine("Copy and paste location of the Changed (After) file");
                string @after = Console.ReadLine();
                after = after.Replace("\"", "");

                Console.WriteLine("0 - Show Diff comparison\n1 - New Value Only\n2 - Old Value\n3 - Show \"Deleted\" cells as Green");
                string @choice = Console.ReadLine();

                int selection = Convert.ToInt32(choice);

                //get a list of worksheets in each file
                List<string> before_sheets = new List<string>();
                List<string> after_sheets = new List<string>();

                Workbook wb_before = new Workbook(before);
                Workbook wb_after = new Workbook(after);

                GetTabs(wb_before, ref before_sheets);
                GetTabs(wb_after, ref after_sheets);


                string a_filename = new FileInfo(before).Name;
                string a_filename_ext = new FileInfo(before).Extension;
                string a_name_only = a_filename.Replace(a_filename_ext, "");
                string b_filename = new FileInfo(after).Name;
                string b_filename_ext = new FileInfo(after).Extension;
                string b_name_only = b_filename.Replace(b_filename_ext, "");
                string ab_name_combined = a_name_only + "_" + b_filename + "_diff.xlsx";
                string path = new FileInfo(after).DirectoryName;
                string full_new_name = Path.Combine(path, ab_name_combined);



                Console.WriteLine();

                //compare lengths
                if (before_sheets.Count > after_sheets.Count)
                {
                    Console.WriteLine("There are more tabs in the Original file!");
                    ProcessTabs(before_sheets, after_sheets, ref wb_before, ref wb_after, full_new_name, selection);
                }
                if (before_sheets.Count == after_sheets.Count)
                {
                    Console.WriteLine("Same amout of tabs");
                    ProcessTabs(before_sheets, after_sheets, ref wb_before, ref wb_after, full_new_name, selection);
                }
                if (before_sheets.Count < after_sheets.Count)
                {
                    Console.WriteLine("There are more tabs in the Changed file!");

                    ProcessTabs(after_sheets, before_sheets, ref wb_before, ref wb_after, full_new_name, selection);
                }


                Console.WriteLine("Done");

            }
            Console.ReadKey();
        }


        static void GetTabs(Workbook wb, ref List<string> ws_list)
        {
            foreach (Worksheet ws in wb.Worksheets)
            {
                if (ws.IsVisible)
                {
                    ws_list.Add(ws.Name);
                }
            }
        }

        static void ProcessTabs(List<string> set1, List<string> set2, ref Workbook wb1, ref Workbook wb2, string outputfile, int selection)
        {


            Style iyellow = new CellsFactory().CreateStyle();
            iyellow.Pattern = BackgroundType.Solid;
            iyellow.ForegroundColor = Color.Yellow;

            Workbook wb = new Workbook();

            string[] sheets = set2.ToArray();
            foreach (string tab in set1)
            {
                bool skip = true;
                for (int i = 0; i < sheets.Length; i++)
                {

                    if (tab == sheets[i])
                    {
                        skip = false;

                    }

                }

                if (skip == true)
                {
                    Console.WriteLine($"Skipping {tab}");
                }
                else
                {
                    Console.WriteLine($"Processing {tab}");

                    Worksheet ws1 = wb1.Worksheets[tab];
                    Worksheet ws2 = wb2.Worksheets[tab];
                    Worksheet ws_out = wb.Worksheets[0];

                    if (tab != "Sheet1")
                    {
                        ws_out = wb.Worksheets.Add(tab);
                    }


                    ws_out = wb.Worksheets[tab];

                    //we should compare against the excel which has the largest number of columns and rows
                    int maxrow = 0;
                    int maxcol = 0;

                    //
                    if (ws1.Cells.MaxDataRow > ws2.Cells.MaxDataRow)
                    {
                        maxrow = ws1.Cells.MaxDataRow;
                    }
                    if (ws1.Cells.MaxDataColumn > ws2.Cells.MaxDataColumn)
                    {
                        maxcol = ws1.Cells.MaxDataColumn;
                    }

                    //
                    if (ws1.Cells.MaxDataRow < ws2.Cells.MaxDataRow)
                    {
                        maxrow = ws2.Cells.MaxDataRow;
                    }
                    if (ws1.Cells.MaxDataColumn < ws2.Cells.MaxDataColumn)
                    {
                        maxcol = ws2.Cells.MaxDataColumn;
                    }

                    //
                    if (ws1.Cells.MaxDataRow == ws2.Cells.MaxDataRow)
                    {
                        maxrow = ws1.Cells.MaxDataRow;
                    }
                    if (ws1.Cells.MaxDataColumn == ws2.Cells.MaxDataColumn)
                    {
                        maxcol = ws1.Cells.MaxDataColumn;
                    }




                    for (int row = 0; row < maxrow + 1; row++)
                    {
                        for (int col = 0; col < maxcol + 1; col++)
                        {
                            if (ws1.Cells[row, col].Value == null && ws2.Cells[row, col].Value != null)
                            {
                                if (ws2.Cells[row, col].Value.ToString() != "")
                                {
                                    //new segment
                                    ws_out.Cells[row, col].SetStyle(iyellow);
                                    if (selection == 0 || selection == 3)
                                    {
                                        ws_out.Cells[row, col].HtmlString = BeforeAndAfter("", ws2.Cells[row, col].Value.ToString(), false);
                                    }
                                    if (selection == 1)
                                    {
                                        ws_out.Cells[row, col].Value = ws2.Cells[row, col].Value.ToString();
                                    }
                                    if (selection == 2)
                                    {
                                        ws_out.Cells[row, col].Value = "";
                                    }
                                }


                            }
                            if (ws1.Cells[row, col].Value != null && ws2.Cells[row, col].Value != null)
                            {
                                if (ws1.Cells[row, col].Value.ToString() == ws2.Cells[row, col].Value.ToString())
                                {
                                    //no change
                                    if (selection == 0 || selection == 3)
                                    {
                                        ws_out.Cells[row, col].HtmlString = ws1.Cells[row, col].Value.ToString();
                                    }
                                    if (selection == 1)
                                    {
                                        ws_out.Cells[row, col].Value = ws1.Cells[row, col].Value.ToString();
                                    }
                                    if (selection == 2)
                                    {
                                        ws_out.Cells[row, col].Value = ws1.Cells[row, col].Value.ToString();
                                    }

                                }
                                else
                                {

                                    //ok so our values don't match ... 
                                    
                                    //changed
                                    ws_out.Cells[row, col].SetStyle(iyellow);

                                    if (selection == 0 || selection == 3)
                                    {
                                        ws_out.Cells[row, col].HtmlString = BeforeAndAfter(ws1.Cells[row, col].Value.ToString(), ws2.Cells[row, col].Value.ToString(), false);
                                    }
                                    if (selection == 1)
                                    {
                                        ws_out.Cells[row, col].Value = ws2.Cells[row, col].Value.ToString();
                                    }
                                    if (selection == 2)
                                    {
                                        ws_out.Cells[row, col].Value = ws1.Cells[row, col].Value.ToString();
                                    }


                                }
                            }

                            if (ws1.Cells[row, col].Value != null && ws2.Cells[row, col].Value == null)
                            {
                                if (ws1.Cells[row, col].Value.ToString() != "")
                                {
                                    //deleted segment
                                    ws_out.Cells[row, col].SetStyle(iyellow);
                                    if (selection == 0)
                                    {
                                        ws_out.Cells[row, col].HtmlString = BeforeAndAfter(ws1.Cells[row, col].Value.ToString(), "", false);

                                    }
                                    if (selection == 1)
                                    {
                                        ws_out.Cells[row, col].Value = "";
                                    }
                                    if (selection == 2)
                                    {
                                        ws_out.Cells[row, col].Value = ws1.Cells[row, col].Value.ToString();
                                    }
                                    if (selection == 3)
                                    {
                                        // the new segment is empty but we want to still show the old segment
                                        ws_out.Cells[row, col].HtmlString = BeforeAndAfter("", ws1.Cells[row, col].Value.ToString(), true);
                                    }
                                }


                            }
                            // if both null we never had a value :p

                        }
                    }


                }

            }


            wb.Save(outputfile);
        }


        static string BeforeAndAfter(string original_segment, string cleaned_segment, bool green)
        {


            diff_match_patch dmp = new diff_match_patch();
            List<Diff> diff = dmp.diff_main(original_segment, cleaned_segment);
            dmp.diff_cleanupSemantic(diff);

            StringBuilder pretty_segment = new StringBuilder();
            for (int ix = 0; ix < diff.Count; ix++)
            {

                string pattern_equals = "(?<=^Diff\\(EQUAL,\")(.*?)(?=\"\\)$)";
                string pattern_delete = "(?<=^Diff\\(DELETE,\")(.*?)(?=\"\\)$)";
                string pattern_insert = "(?<=^Diff\\(INSERT,\")(.*?)(?=\"\\)$)";

                var segment_equal = Regex.Match(diff[ix].ToString(), pattern_equals);
                var segment_delete = Regex.Match(diff[ix].ToString(), pattern_delete);
                var segment_insert = Regex.Match(diff[ix].ToString(), pattern_insert);

                if (segment_equal.Success)
                {
                    string sentence = segment_equal.Value;
                    sentence = Regex.Replace(sentence, "¶", "\n");
                    sentence = Regex.Replace(sentence, "&", "&amp;");
                    sentence = Regex.Replace(sentence, "<", "&lt;");
                    sentence = Regex.Replace(sentence, ">", "&gt;");
                    sentence = Regex.Replace(sentence, "&nbsp;", "&amp;nbsp;");
                    //Console.WriteLine(sentence);
                    pretty_segment.Append(sentence);
                }

                if (segment_insert.Success)
                {
                    string sentence = segment_insert.Value;
                    sentence = Regex.Replace(sentence, "¶", "\n");
                    sentence = Regex.Replace(sentence, "<", "&lt;");
                    sentence = Regex.Replace(sentence, ">", "&gt;");
                    sentence = Regex.Replace(sentence, "&nbsp;", "&amp;nbsp;");
                    //sentence = sentence.Replace(" ", "(SPACE)");
                    if (green == true)
                    {
                        sentence = "<span style=\"color:#0b8c30;font-weight: bold;\">" + sentence + "</span>";
                    }
                    else
                    {
                        sentence = "<span style=\"color:#0000ff;font-weight: bold;\">" + sentence + "</span>";
                    }


                    pretty_segment.Append(sentence);
                }

                if (segment_delete.Success)
                {
                    string sentence = segment_delete.Value;
                    sentence = Regex.Replace(sentence, "¶", "\n");
                    sentence = Regex.Replace(sentence, "<", "&lt;");
                    sentence = Regex.Replace(sentence, ">", "&gt;");
                    sentence = Regex.Replace(sentence, "&nbsp;", "&amp;nbsp;");
                    //sentence = sentence.Replace(" ", "(SPACE)");
                    sentence = sentence.Replace("\r", "x&#48;&#48;&#48;d");

                    sentence = "<span style=\"color:#ff0000;text-decoration: line-through;font-weight: bold;\">" + sentence + "</span>";
                    //Console.WriteLine(sentence);
                    pretty_segment.Append(sentence);
                }

            }

            return pretty_segment.ToString();

        }

    }



}
